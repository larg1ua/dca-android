package com.larryrider.dca;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.larryrider.dca.Extra.Comentario;
import com.larryrider.dca.Extra.Error;

import java.util.ArrayList;

public class CrearError extends Actividad {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_error);

        Spinner gravedad = (Spinner) findViewById(R.id.spinner_gravedad);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.tipos_error, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        gravedad.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        cambiarActividad(this, new Intent(this, VerListaErrores.class));
    }

    private Boolean comprobarCampos() {
        ArrayList<EditText> editTexts = new ArrayList<>();
        editTexts.add((EditText) findViewById(R.id.editTextTitulo));
        editTexts.add((EditText) findViewById(R.id.editTextUsuario));
        editTexts.add((EditText) findViewById(R.id.editTextInformacion));
        editTexts.add((EditText) findViewById(R.id.editTextProgramadores));
        editTexts.add((EditText) findViewById(R.id.editTextEstado));
        editTexts.add((EditText) findViewById(R.id.editTextExplicacion));
        for (EditText e : editTexts) {
            if (e.getText().toString().contentEquals("")) {
                e.setError("Este campo no puede estar vacio");
                return false;
            }
        }
        return true;
    }

    public void crearError(View v) {
        if (comprobarCampos()) {
            String titulo = ((EditText) findViewById(R.id.editTextTitulo)).getText().toString();
            String gravedad = ((Spinner) findViewById(R.id.spinner_gravedad)).getSelectedItem().toString();
            String usuario = ((EditText) findViewById(R.id.editTextUsuario)).getText().toString();
            String informacion = ((EditText) findViewById(R.id.editTextInformacion)).getText().toString();
            String programadores = ((EditText) findViewById(R.id.editTextProgramadores)).getText().toString();
            String estado = ((EditText) findViewById(R.id.editTextEstado)).getText().toString();
            String explicacion = ((EditText) findViewById(R.id.editTextExplicacion)).getText().toString();

            errores.add(new Error(titulo, gravedad, usuario, informacion, programadores, estado, explicacion, new ArrayList<Comentario>()));

            cambiarActividad(this, new Intent(this, VerListaErrores.class));
        }
    }
}

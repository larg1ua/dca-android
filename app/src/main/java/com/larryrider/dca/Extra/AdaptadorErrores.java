package com.larryrider.dca.Extra;

/**
 * Created by Larry on 04/10/2017.
 */

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.larryrider.dca.R;

import java.util.ArrayList;


public class AdaptadorErrores extends ArrayAdapter<Error> {
    private Context context;
    private int layoutId;
    private ArrayList<Error> errores = new ArrayList<>();

    public AdaptadorErrores(Context context, int resource, ArrayList<Error> errores) {
        super(context, resource, errores);
        this.context = context;
        this.layoutId = resource;
        this.errores = errores;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View item = convertView;
        ObjetoWrapper objetoWrapper;
        if (item == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            item = inflater.inflate(layoutId, parent, false);
            objetoWrapper = new ObjetoWrapper();
            objetoWrapper.titulo = (TextView) item.findViewById(R.id.titulo);
            objetoWrapper.usuario = (TextView) item.findViewById(R.id.usuario);
            objetoWrapper.estado = (TextView) item.findViewById(R.id.estado);
            objetoWrapper.informacion = (TextView) item.findViewById(R.id.informacion);

            item.setTag(objetoWrapper);
        } else {
            objetoWrapper = (ObjetoWrapper) item.getTag();
        }

        Error actual = errores.get(position);

        objetoWrapper.titulo.setText(actual.getTitulo());
        objetoWrapper.usuario.setText(actual.getUsuario());
        objetoWrapper.estado.setText(actual.getEstado());
        objetoWrapper.informacion.setText(actual.getInformacion());

        if(actual.getEstado().contentEquals("Cerrado") || actual.getEstado().contentEquals("CERRADO") || actual.getEstado().contentEquals("cerrado")){
            objetoWrapper.estado.setTextColor(Color.RED);
        }

        return item;
    }

    private static class ObjetoWrapper {
        TextView titulo, usuario, estado, informacion;
    }


}


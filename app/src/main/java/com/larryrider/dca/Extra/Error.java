package com.larryrider.dca.Extra;

import java.util.ArrayList;

public class Error {
    private String titulo; //Titulo del fallo
    private String gravedad; //Gravedad del fallo
    private String usuario; //Usuario que lo ha detectado
    private String informacion; //Como reproducir el fallo
    private String programadores; //Los que intervienen en su resolucion
    private String estado; //Estado actual
    private String explicacion; //Por qué el usuario piensa que es un fallo
    private ArrayList<Comentario> comentarios;

    public Error(String titulo, String gravedad, String usuario, String informacion, String programadores, String estado, String explicacion, ArrayList<Comentario> comentarios) {
        this.titulo = titulo;
        this.gravedad = gravedad;
        this.usuario = usuario;
        this.informacion = informacion;
        this.programadores = programadores;
        this.estado = estado;
        this.explicacion = explicacion;
        this.comentarios = comentarios;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getGravedad() {
        return gravedad;
    }

    public void setGravedad(String gravedad) {
        this.gravedad = gravedad;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getInformacion() {
        return informacion;
    }

    public void setInformacion(String informacion) {
        this.informacion = informacion;
    }

    public String getProgramadores() {
        return programadores;
    }

    public void setProgramadores(String programadores) {
        this.programadores = programadores;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getExplicacion() {
        return explicacion;
    }

    public void setExplicacion(String explicacion) {
        this.explicacion = explicacion;
    }

    public ArrayList<Comentario> getComentarios() {
        return comentarios;
    }

    public void setComentarios(ArrayList<Comentario> comentarios) {
        this.comentarios = comentarios;
    }
}

package com.larryrider.dca.Extra;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.larryrider.dca.R;

import java.util.ArrayList;

/**
 * Created by Larry on 04/10/2017.
 */

public class AdaptadorComentarios extends ArrayAdapter<Comentario> {
    private Context context;
    private int layoutId;
    private ArrayList<Comentario> comentarios = new ArrayList<>();

    public AdaptadorComentarios(Context context, int resource, ArrayList<Comentario> comentarios) {
        super(context, resource, comentarios);
        this.context = context;
        this.layoutId = resource;
        this.comentarios = comentarios;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View item = convertView;
        ObjetoWrapper objetoWrapper;
        if (item == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            item = inflater.inflate(layoutId, parent, false);
            objetoWrapper = new ObjetoWrapper();
            objetoWrapper.usuario = (TextView) item.findViewById(R.id.usuario);
            objetoWrapper.comentario = (TextView) item.findViewById(R.id.comentario);

            item.setTag(objetoWrapper);
        } else {
            objetoWrapper = (ObjetoWrapper) item.getTag();
        }

        Comentario actual = comentarios.get(position);

        objetoWrapper.usuario.setText(actual.getUsuario());
        objetoWrapper.comentario.setText(actual.getComentario());

        return item;
    }

    private static class ObjetoWrapper {
        TextView usuario, comentario;
    }
}

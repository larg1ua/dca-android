package com.larryrider.dca;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.larryrider.dca.Extra.AdaptadorErrores;

public class VerListaErrores extends Actividad {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ver_lista_errores);


        ListView listView = (ListView) findViewById(R.id.listaErrores);
        AdaptadorErrores adaptadorErrores = new AdaptadorErrores(this, R.layout.fila_errores, errores);
        if (errores.size() == 0) {
            ((TextView) findViewById(R.id.textoNoErrores)).setText("No hay errores");
        }
        listView.setScrollingCacheEnabled(false);
        listView.setAdapter(adaptadorErrores);
    }

    @Override
    public void onBackPressed() {
        cambiarActividad(this, new Intent(this, PaginaPrincipal.class));
    }

    public void crearError(View v) {
        cambiarActividad(this, new Intent(this, CrearError.class));
    }

    public void verError(View v) {
        v = (View) v.getParent();
        ListView listView = (ListView) v.getParent();
        int posicion = listView.getPositionForView(v);

        cambiarActividad(this, new Intent(this, VerError.class).putExtra("posicion", posicion));
    }
}

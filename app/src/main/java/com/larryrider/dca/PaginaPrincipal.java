package com.larryrider.dca;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.View;

import com.larryrider.dca.Extra.Comentario;
import com.larryrider.dca.Extra.Error;

import java.util.ArrayList;

public class PaginaPrincipal extends Actividad {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pagina_principal);
    }

    public void empezar(View v) {
        errores.clear();
        cambiarActividad(this, new Intent(this, VerListaErrores.class));
    }

    public void empezarConLista(View v) {
        errores.clear();
        inicializarErrores();
        cambiarActividad(this, new Intent(this, VerListaErrores.class));
    }

    private void inicializarErrores() {
        Comentario comentario1 = new Comentario("Juan", "Me pongo a ello");
        Comentario comentario2 = new Comentario("Larry", "Creo que se como solucionarlo");
        Comentario comentario3 = new Comentario("Pepe", "Es un bug muy extraño, se lo paso a mis superiores");
        ArrayList<Comentario> comentarioArrayList1 = new ArrayList<>();
        comentarioArrayList1.add(comentario1);
        comentarioArrayList1.add(comentario2);
        comentarioArrayList1.add(comentario3);
        ArrayList<Comentario> comentarioArrayList2 = new ArrayList<>();
        comentarioArrayList2.add(comentario1);
        comentarioArrayList2.add(comentario2);
        comentarioArrayList2.add(comentario3);
        ArrayList<Comentario> comentarioArrayList3 = new ArrayList<>();
        comentarioArrayList3.add(comentario1);
        comentarioArrayList3.add(comentario2);
        comentarioArrayList3.add(comentario3);

        Error error1 = new Error("Bug 1", "Normal", "Juan", "No funciona el intro", "Alberto y Larry", "ABIERTO", "Cuando pulso en el teclado el intro no funciona", comentarioArrayList1);
        Error error2 = new Error("Bug X", "Important", "Pepe", "No actualiza la app", "Larry", "EN PROCESO", "El sistema de actualizacion no funciona correctamente", comentarioArrayList2);
        Error error3 = new Error("Error 523", "Critical", "Pepe", "Se bloquea la app", "Jaime", "CERRADO", "La app se bloquea cuando pulsas en el boton de cambiar", comentarioArrayList3);

        errores.add(error1);
        errores.add(error2);
        errores.add(error3);
    }

    @Override
    public void onBackPressed() {
        ActivityCompat.finishAffinity(this);
        this.finish();
    }
}

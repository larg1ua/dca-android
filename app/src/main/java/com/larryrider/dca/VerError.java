package com.larryrider.dca;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.larryrider.dca.Extra.AdaptadorComentarios;
import com.larryrider.dca.Extra.Comentario;

import java.util.ArrayList;

public class VerError extends Actividad {

    private Integer posicion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ver_error);

        posicion = getIntent().getExtras().getInt("posicion");

        ((TextView) findViewById(R.id.titulo)).setText(errores.get(posicion).getTitulo());
        ((TextView) findViewById(R.id.usuario)).setText(errores.get(posicion).getUsuario());
        ((TextView) findViewById(R.id.estado)).setText(errores.get(posicion).getEstado());
        ((TextView) findViewById(R.id.gravedad)).setText(errores.get(posicion).getGravedad());
        ((TextView) findViewById(R.id.explicacion)).setText(errores.get(posicion).getExplicacion());
        ((TextView) findViewById(R.id.programadores)).setText(errores.get(posicion).getProgramadores());

        if (errores.get(posicion).getEstado().contentEquals("CERRADO") || errores.get(posicion).getGravedad().contentEquals("cerrado") || errores.get(posicion).getGravedad().contentEquals("Cerrado")) {
            ((TextView) findViewById(R.id.estado)).setTextColor(Color.RED);
            findViewById(R.id.cerrarError).setVisibility(View.GONE);
            findViewById(R.id.nuevoComentario).setVisibility(View.GONE);
        }

        ListView listView = (ListView) findViewById(R.id.listaComentarios);
        AdaptadorComentarios adaptadorComentarios = new AdaptadorComentarios(this, R.layout.fila_comentarios, errores.get(posicion).getComentarios());
        if (errores.get(posicion).getComentarios().size() == 0) {
            findViewById(R.id.textoNoComentarios).setVisibility(View.VISIBLE);
            ((TextView) findViewById(R.id.textoNoComentarios)).setText("No hay comentarios");
        }
        listView.setScrollingCacheEnabled(false);
        listView.setAdapter(adaptadorComentarios);

    }

    @Override
    public void onBackPressed() {
        cambiarActividad(this, new Intent(this, VerListaErrores.class));
    }

    public void cerrarError(View v) {
        errores.get(posicion).setEstado("CERRADO");
        onBackPressed();
    }

    public void nuevoComentario(View v) {
        if (findViewById(R.id.layout_comentar).getVisibility() == View.VISIBLE) {
            findViewById(R.id.layout_comentar).setVisibility(View.GONE);
            findViewById(R.id.comentarios).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.layout_comentar).setVisibility(View.VISIBLE);
            findViewById(R.id.comentarios).setVisibility(View.GONE);
        }
    }

    public void comentar(View v) {
        if (comprobarCampos()) {
            String usuario = ((TextView) findViewById(R.id.editTextUsuario)).getText().toString();
            String comentario = ((TextView) findViewById(R.id.editTextComentario)).getText().toString();
            errores.get(posicion).getComentarios().add(new Comentario(usuario, comentario));
            findViewById(R.id.layout_comentar).setVisibility(View.GONE);
            findViewById(R.id.comentarios).setVisibility(View.VISIBLE);
            findViewById(R.id.textoNoComentarios).setVisibility(View.GONE);
        }
    }

    private Boolean comprobarCampos() {
        ArrayList<EditText> editTexts = new ArrayList<>();
        editTexts.add((EditText) findViewById(R.id.editTextUsuario));
        editTexts.add((EditText) findViewById(R.id.editTextComentario));

        for (EditText e : editTexts) {
            if (e.getText().toString().contentEquals("")) {
                e.setError("Este campo no puede estar vacio");
                return false;
            }
        }
        return true;
    }
}

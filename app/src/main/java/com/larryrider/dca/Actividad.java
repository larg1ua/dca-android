package com.larryrider.dca;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;

import com.larryrider.dca.Extra.Error;

import java.util.ArrayList;

public class Actividad extends AppCompatActivity {

    public static ArrayList<Error> errores;

    @Override
    protected void onStart() {
        super.onStart();
        if(errores == null){
            errores = new ArrayList<>();
        }
    }

    public static void cambiarActividad(Activity from, Intent intent) {
        if (from != null) {
            from.startActivity(intent);
            ActivityCompat.finishAffinity(from);
            from.finish();
        }
    }
}
